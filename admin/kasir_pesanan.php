<?php include_once 'cek_login.php';?>
<?php include 'function/connect.php'; ?>
<?php include_once 'function/admin/admin_pesanan.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Kasir
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header"><a href="admin_change_password.php">Change Password</a></li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  		
  	<div class="col-md-12 content">
  		<div class="panel panel-default">
            <div class="panel-heading">
                Daftar Pesanan
            </div>
        	<div class="panel-body">
                <!-- admin_add_edit_kategori.php?kategori=edit&id=id -->
                <div class="row">
                    <div class="col-md-12">
                        <br>      
                          <table class="table-bordered table-condensed table-striped table" style="border-collapse:collapse;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <!-- <th>Kode Produk</th> -->
                                    <th>Member</th>
                                    <th>Tipe Pesanan</th>
                                    <th>Tanggal Pesanan</th>
                                    <th width="25%">Info Dine/Take/Deliv</th>
                                    <th>Konfirmasi Oleh</th>
                                    <th>Tanggal Konfirmasi</th>
                                    <th>Status Pesanan</th>
                                    <th width="18%"></th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php 
                                    $queryPesanan=getPesanan();
                                    $no=1;
                                    $total_bayar=0;
                                    while($resultQueryPesanan=mysql_fetch_array($queryPesanan)){
                              ?>  
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo strtoupper($resultQueryPesanan['nama_depan'])." ".strtoupper($resultQueryPesanan['nama_belakang']).'<br>Email '. $resultQueryPesanan['email'] ; ?></td>
                                        <td><?php echo strtoupper($resultQueryPesanan['nama_tipe_pesanan']); ?></td>
                                        <td><?php echo $resultQueryPesanan['tanggal_pesan']; ?></td>
                                        <td>
                                            <?php
                                                // if($resultQueryPesanan['nama_tipe_pesanan']=='take away'){
                                                //     $get_take=getDateTakeAway($resultQueryPesanan['id_pesanan']);
                                                //     $take=mysql_fetch_assoc($get_take);
                                                //     // print_r($get_take);
                                                //     echo $take['jam_ambil_pesanan'];
                                                // }
                                                
                                                if($resultQueryPesanan['nama_tipe_pesanan']=='dine in'){
                                                    $dine_in=getDateDineIn($resultQueryPesanan['id_pesanan']);
                                                    $time_dine=mysql_fetch_assoc($dine_in);
                                                    echo '<strong> Jam Datang    &nbsp;&nbsp;:</strong>'.$time_dine['jam_datang'];
                                                    echo '<br> <strong> No Hp        &nbsp;&nbsp;&nbsp;: </strong>'.$resultQueryPesanan['no_telpon'];
                                                   
                                                    // print_r($get_take);
                                                }

                                                if($resultQueryPesanan['nama_tipe_pesanan']=='take away'){
                                                    $get_take=getDateTakeAway($resultQueryPesanan['id_pesanan']);
                                                    $take=mysql_fetch_assoc($get_take);
                                                    // print_r($get_take);
                                                    echo '<strong> Jam Ambil    &nbsp;&nbsp;:</strong>'.$take['jam_ambil_pesanan'];
                                                    echo '<br> <strong> No Hp        &nbsp;&nbsp;&nbsp;: </strong>'.$resultQueryPesanan['no_telpon'];
                                                   
                                                }
                                                
                                                if($resultQueryPesanan['nama_tipe_pesanan']=='delivery'){
                                                    $delivery=getDateDelivery($resultQueryPesanan['id_pesanan']);
                                                    $time_delivery=mysql_fetch_assoc($delivery);
                                                    echo '<strong> Jam Antar    &nbsp;&nbsp;:</strong>'.$time_delivery['jam_antar_pesanan'];
                                                    echo '<br> <strong> No Hp        &nbsp;&nbsp;&nbsp;: </strong>'.$time_delivery['no_hp'];
                                                    echo '<br> <strong> Alamat Antar :</strong>'.$time_delivery['alamat_antar'];
                                                    // print_r($get_take);
                                                }
                                            
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($resultQueryPesanan['id_admin']!=null){
                                                    $nama_admin=mysql_fetch_assoc(getAdmin($resultQueryPesanan['id_admin']));
                                                    echo $nama_admin['nama']; 
                                                }
                                            ?>
                                        </td>
                                        <td><?php echo $resultQueryPesanan['tgl_konfirmasi']; ?></td>
                                        <td><?php echo $resultQueryPesanan['nama_status_pesanan']; ?></td>
                                        <td>
                                            <?php if($resultQueryPesanan['nama_status_pesanan']=='kadaluarsa' ){ ?>
                                                <span class="btn btn-warning">Kadaluarsa</span>
                                            <?php }if($resultQueryPesanan['nama_status_pesanan']=='sudah konfirmasi'){ ?>
                                                <span class="btn btn-info">Sudah Konfirmasi</span>
                                            <?php
                                            }
                                            if($resultQueryPesanan['nama_status_pesanan']!='sudah konfirmasi'){
                                            ?>
                                                <a href="function/admin/admin_pesanan.php?konfirmasi=<?php  echo $resultQueryPesanan["id_pesanan"]; ?>" class="btn btn-success">Konfirmasi</a>
                                           
                                            <?php } ?>
                                             | <span data-toggle="collapse" data-target="#kasir_pesanan<?php echo $no; ?>" class="clickable btn btn-default"> Detail </span></td>
                                    </tr>  
                                    <tr class="collapse" id="kasir_pesanan<?php echo $no; ?>">
                                    <td colspan="9">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Detail Menu Pesanan</h4>
                                                </div>
                                            </div>
                                            <?php
                                                  $sqlPesananDetail=pesananDetail($resultQueryPesanan['id_pesanan']);
                                                  $jumlah=0;
                                                  $total_bayar=0;
                                                  while ($querySqlPesananDetail=mysql_fetch_array($sqlPesananDetail)) {
                                           
                                            ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3"><?php echo $querySqlPesananDetail['nama_menu']; ?></div>
                                                    <div class="col-md-3"> @ <?php echo $querySqlPesananDetail['harga']; ?></div>
                                                    <div class="col-md-3">Jumlah: <?php echo $querySqlPesananDetail['jumlah']; ?>  Total_Harga: <?php $total_bayar +=$querySqlPesananDetail['jumlah']*$querySqlPesananDetail['harga']; echo $querySqlPesananDetail['jumlah']*$querySqlPesananDetail['harga']; ?> </div>
                                                    <div class="col-md-3">Catatan <br>
                                                      <?php echo $querySqlPesananDetail['catatan_tambahan']; ?>
                                                    </div>
                                                </div>
                                            </div>    
                                            <?php
                                                    }
                                            ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-3"><h3>Total Bayar: <?php echo $total_bayar; ?> </h3></div>
                                                    <div class="col-md-3"><h3></div>
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-3"></div>
                                                </div>
                                            </div>                                              
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  				Copyright &COPY; 2015 <a href="http://www.pingpong-labs.com">Gravitano</a>
  			</p>
  		</footer>
  	</div>
</body>
</html>