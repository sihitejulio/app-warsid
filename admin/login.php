<?php
  session_start();
  $sesi_user= isset($_SESSION['admin']) ? $_SESSION['admin'] : NULL;
  if ($sesi_user != NULL || !empty($sesi_user))
  {
    header("Location:index.php");
  }
 ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Admin Interface</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="Theme/css/AdminInt.css">
        <script src="Theme/js/AdminInt.js"></script>
    </head>

    <body>
    <div class="container">
      <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-12" style="margin-top:50px;margin-left:350px;">
              <div class="sales">
                <form class="form-horizontal" action="function/admin/admin_login.php" method="POST">
                  <?php
                    if (isset($_GET['login'])) {
                      if($_GET['login']=="false"){?>
                      <div class="alert alert-warning" role="alert">! Login Gagal</div>
                  <?php
                        }
                      }
                  ?>

                    <fieldset>

                    <!-- Form Name -->
                    <legend>Login Admin</legend>

                     <!-- Text input-->
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Username</label>  
                        <div class="col-md-5">
                        <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md">
                        <!-- <span class="help-block">help</span>   -->
                        </div>
                     </div>

                    <!-- Password input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="password">Password</label>
                      <div class="col-md-5">
                        <input id="password" name="password" type="password" placeholder="password" class="form-control input-md">
                        <!-- <span class="help-block">help</span> -->
                      </div>
                    </div>

                   

                    <!-- Button (Double) -->
                      <div class="form-group">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-8">
                            <button id="login" type="submit" name="login" class="btn-block btn btn-success">Login</button>
                        </div>
                      </div>
                    </fieldset>
                  </form>
              </div>
          </div>
        </div>
    </div>
</body>
</html>
