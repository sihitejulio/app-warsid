<?php include_once 'cek_login.php';?>
<?php include 'function/connect.php'; ?>
<?php include_once 'function/admin/admin_pesanan.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Change Password 
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header"><a href="admin_change_password.php">Change Password</a></li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  	<div class="col-md-3">
    </div>
  	<div class="col-md-6">
  		<div class="panel panel-default">
            <div class="panel-heading">
            </div>
        	<div class="panel-body">
                <!-- admin_add_edit_kategori.php?kategori=edit&id=id -->
                <div class="row">
                    <div class="col-md-12">
                        <br>      
                        <form role="Form" method="POST" action="function/admin/admin_changepass.php" accept-charset="UTF-8">
                            <div class="form-group">
                                <input type="password" name="pass_old" placeholder="Old Password..." class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="pass_old_ver" placeholder="Verify Old password..." class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="pass_new" placeholder="New Password..." class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" name="pass_new_ver" placeholder="Verify New password..." class="form-control">
                            </div>
                            <div class="form-group">
                                <button type="submit" name="change_password" class="btn btn-default">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  			</p>
  		</footer>
  	</div>
</body>
</html>