<?php include_once 'cek_login.php';?>
<?php include 'function/connect.php'; ?>
<?php include 'function/admin/admin_member.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Interface</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="Theme/css/AdminInt.css">
    <script src="Theme/js/AdminInt.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
			MENU
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				Administrator
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
			
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown ">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						Account
						<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-header"><a href="admin_change_password.php">Change Password</a></li>
							<li class="divider"></li>
							<li><a href="function/admin/admin_logout.php?logout=true">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>  	
         
    <div class="container-fluid main-container">
  		<div class="col-md-2 sidebar">
  			<div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <?php include "side_menu.php"; ?>
                </nav>
            </div>
        </div>  		
    </div>
  	<div class="col-md-10 content">
  		<div class="panel panel-default">
            <div class="panel-heading">
                Daftar Member
            </div>
        	<div class="panel-body">
               
                <div class="row">
                    <div class="col-md-12">
                    <br>
                        
                    <table class="table-bordered table-condensed table-striped table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <!-- <th>Kode Produk</th> -->
                                <th>Nama Depan</th>
                                <th>Nama Belakang</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Password</th>
                                <th>No Virtual Account</th>
                                <th>Saldo</th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $queryAllMember=getAllMember();  
                                $no=1;                          
                                while ($resultQueryMember=mysql_fetch_array($queryAllMember)) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $resultQueryMember['nama_depan']; ?></td>
                                    <td><?php echo $resultQueryMember['nama_belakang']; ?></td>
                                    <td><?php echo $resultQueryMember['email']; ?></td>
                                    <td><?php echo $resultQueryMember['alamat']; ?></td>
                                    <td><?php echo $resultQueryMember['password']; ?></td>
                                    <td><?php echo $resultQueryMember['no_virtual_account']; ?></td>
                                    <td><?php echo $resultQueryMember['saldo']; ?></td>
                                    <td class="text-center">
                                        <?php
                                            if($resultQueryMember['status_user']=='0'){
                                        ?>
                                               <a class="btn btn-success" href="function/admin/admin_member.php?aktif=<?php echo $resultQueryMember['id']; ?>">  Aktifkan </a></td>
                                               <?php
                                            }elseif($resultQueryMember['status_user']=='2'){
                                        ?>
                                               <a class="btn btn-info" href="function/admin/admin_member.php?verify=<?php echo $resultQueryMember['id']; ?>">  Verifikasi </a></td>

                                        <?php
                                            }else{
                                        ?>
                                                <a class="btn btn-danger" href="function/admin/admin_member.php?block=<?php echo $resultQueryMember['id']; ?>">  Non Aktifkan </a></td>
                                        <?php
                                            }
                                        ?>
                                </tr>           
                            <?php
                            }?>
                        </tbody>
                    </table>
                    </div>	
                </div>
	        </div>
        </div>
  		</div>
  		<footer class="pull-left footer">
  			<p class="col-md-12">
  				<hr class="divider">
  				Copyright &COPY; 2015 <a href="http://www.pingpong-labs.com">Gravitano</a>
  			</p>
  		</footer>
  	</div>
</body>
</html>