

<?php include "function/connect.php"; ?>
<?php include "function/va.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="container" style="margin-top:50px;">
        <div class="row">
            <div class="col-md-4">
            </div>

            <div class="col-md-4">
                <h4>Transfer Ke Virtual Account</h4>
                <form role="Form" method="POST" action="function/va.php" accept-charset="UTF-8">
                    <div class="form-group">
                        <input type="text" name="no_va" placeholder="Nomor Virtual Account" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" name="nominal" placeholder="Nominal" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="transfer" class="btn btn-default">Transfer</button>
                    </div>
				</form>
            </div>
            <div class="col-md-4">    
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>NO VA</td>
                            <td>Nominal</td>
                            <td>Tanggal Transfer</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sql_trf=getAllVa();
                        while ($querySqlTrf=mysql_fetch_array($sql_trf)) {
                    ?>
                        <tr>
                            <td><?php echo $querySqlTrf['no_va']; ?></td>
                            <td><?php echo $querySqlTrf['nominal']; ?></td>
                            <td><?php echo $querySqlTrf['tgl_transfer']; ?></td>
                        </tr>
                    <?php 
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>