<!DOCTYPE html>
<html lang="en">
<head>
    <title>Change Password</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>

<body>
<?php include "navigation_user.php"; ?>

    <div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			<div class="tab-content">
				<div id="changepass" class="tab-pane fade  fade in active">
					<h3>Change Password</h3>
					<p>Tempat anda mengganti password lama anda</p>
					<form role="Form" method="POST" action="function/member/member_changepass.php" accept-charset="UTF-8">
						<div class="form-group">
							<input type="password" name="pass_old" placeholder="Old Password..." class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="pass_old_ver" placeholder="Verify Old password..." class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="pass_new" placeholder="New Password..." class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="pass_new_ver" placeholder="Verify New password..." class="form-control">
						</div>
						<div class="form-group">
							<button type="submit" name="change_password" class="btn btn-default">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
    <?php include "footer.php"; ?>
    <?php
        if(isset($_GET['pass_old'])){
            if($_GET['pass_old']=='false'){
    ?>
        <script>
            alert('Verifikasi Password Lama Tidak Sesuai');
            window.location.href = 'http://localhost/resto/app/change_password.php'; 
            
        </script>
    <?php
            }
        }
    ?>
     <?php
        if(isset($_GET['pass_new'])){
            if($_GET['pass_new']=='false'){
    ?>
        <script>
            alert('Verifikasi Password Baru Tidak Sesuai');
            window.location.href = 'http://localhost/resto/app/change_password.php'; 
            
        </script>
    <?php
            }
        }
    ?>
     <?php
        if(isset($_GET['pass'])){
            if($_GET['pass']=='not_valid'){
    ?>
        <script>
            alert('Masih Ada Yang Belum Di isi');
            window.location.href = 'http://localhost/resto/app/change_password.php';             
        </script>
    <?php
            }
        }
    ?>
     <?php
        if(isset($_GET['pass'])){
            if($_GET['pass']=='valid'){
    ?>
        <script>
            alert('Berhasil Ganti Passowrd');
            window.location.href = 'http://localhost/resto/app/change_password.php'; 
        </script>
    <?php
            }
        }
    ?>

    <?php
        if(isset($_GET['pass'])){
            if($_GET['pass']=='tidak_sesuai'){
    ?>
        <script>
            alert('Password Tidak Sesuai');
            window.location.href = 'http://localhost/resto/app/change_password.php'; 
        </script>
    <?php
            }
        }
    ?>
    
</body>
</html>