<?php 
    include_once 'function/connect.php';    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
    
</head>

<body>
    <?php include_once "navigation_user.php"; ?>
    <div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			<div class="tab-content">    
				<div id="signup" class="tab-pane fade fade in active">
					<h3>Register</h3>
					<p>Tempat kamu membuat account.</p>
					<form role="Form" method="POST" action="function/member/member_register.php" accept-charset="UTF-8">
					    <div class="form-group">
							<input type="text" name="ndepan" placeholder="Nama Depan" class="form-control">
						</div>
						<div class="form-group">
							<input type="text" name="nbelakang" placeholder="Nama Belakang" class="form-control">
						</div>
						<div class="form-group">
							<input type="text" name="alamat" placeholder="Alamat" class="form-control">
						</div>
						<div class="form-group">
							<input type="text" name="notlp" placeholder="No Telepon" class="form-control">
						</div>
						<div class="form-group">
							<input type="text" name="email" placeholder="Email..." class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="password" placeholder="Password..." class="form-control">
						</div>
						<div class="form-group">
							<input type="password" name="password2" placeholder="Verify password..." class="form-control">
						</div>
						<div class="form-group">
							<button type="submit" name="register" class="btn btn-default">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

    <footer>
        <div class="container">
            <div class="row text-center">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Tentang APS</span>
                        <li>
                            <a href="ProAPS.html">Profile APS</a>
                        </li>

                        <li>
                            <a href="LokOut.html">Lokasi Outlet</a>
                        </li>

                        <li>
                            <a href="Abous.html">About Us</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="menu">
                        <span>Customer Service</span>
                        <li>
                            <a href="JamOP.html">Jam Operasional</a>
                        </li>

                        <li>
                            <a href="HubKami.html">Hubungi Kami</a>
                        </li>

                        <li>
                            <a href="SarKom.html">Saran dan Komentar</a>
                        </li>
                        <li>
                            <a href="FAQ.html">FAQ</a>
                        </li>
                    </ul>
                </div>



                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <ul class="adress">
                        <span>Policies</span>
                        <li>
                            <a href="KebPri.html">Kebijakan Privasi</a>
                        </li>

                        <li>
                            <a href="SyaKet.html">Syarat dan Ketentuan</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-lg-4 col-lg-4 col-xs-12" id="icons">
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#"><i class="fa fa-tumblr"></i></a>
                    </div>
                </div>


            </div>
        </div>
    </footer>
    <?php include_once "footer.php";?>
</body>
</html>