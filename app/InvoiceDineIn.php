<?php
    include_once 'function/connect.php';    
	include_once "function/member/member_riwayat.php";
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include "head.php"; ?>
	<!-- <link rel="stylesheet" href="Theme/css/Index.css"> -->
    <link rel="stylesheet" href="Theme/css/Invoice.css">
	
</head>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
			    <?php
					$id=$_GET['id_pesanan'];
					$pesanan=mysql_fetch_assoc(getPesanan($id));
				?>
    			<h2>Bukti Pemesanan Dine In</h2><h3 class="pull-right">Order # <?php echo $pesanan['id']; ?></h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Ditagihkan Kepada:</strong><br>
						<?php echo $pesanan['nama_depan']. " " . $pesanan['nama_belakang']; ?><br>
					
							<!-- 082182737576<br> -->
							<!-- 144042<br> -->
						<?php echo strtoupper($pesanan['alamat']); ?><br>
    				</address>
					<div>
						<!-- delive info -->
						<h4>Info Dine In</h4>
						<?php
							  $dine_in=getDineIn($pesanan['id_pesanan']);
							  $dinein=mysql_fetch_assoc($dine_in);
							  echo '<strong> Tamu        &nbsp;&nbsp;&nbsp;: </strong>'.$dinein['jumlah_tamu'].' Orang';						
							  echo '<br><strong> No Hp        &nbsp;&nbsp;&nbsp;: </strong>'.$pesanan['no_telpon'];						
						?>
					</div>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong></strong><br>
    					<br>
    					<br>
    					<br>
    					<br>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Pembayaran Dengan:</strong><br>
    					Virtual Account<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Tanggal Order:</strong><br>
						<?php echo strtoupper($pesanan['tanggal_pesan']); ?><br>
						<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Pesanan</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Tambahan</strong></td>
        							<td class="text-center"><strong>Harga</strong></td>
        							<td class="text-center"><strong>Jumlah</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>

								
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<?php
									  $sqlDetail=pesananDetail($id);
									  $no=1;
									  $total=0;
									  while ($queryAll=mysql_fetch_array($sqlDetail)) {

								
								?>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
									<tr>
										<td><?php echo $queryAll['nama_menu']; ?></td>
										<td class="text-center"><?php echo $queryAll['catatan_tambahan']; ?></td>
										<td class="text-center"><?php echo $queryAll['harga']; ?></td>
										<td class="text-center"><?php echo $queryAll['jumlah']; ?></td>
										<td class="text-right"><?php   $sub=$queryAll['jumlah']*$queryAll['harga']; $total+=$sub; echo $sub; ?></td>
									</tr>
								<?php } ?>
    							<!-- <tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right">Rp. 140.000,-</td>
    							</tr> -->
    							<!-- <tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Shipping</strong></td>
    								<td class="no-line text-right">Rp. 10.000,-</td>
    							</tr> -->
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right"><?php echo $total; ?></td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>

<?php include "footer.php"; ?>

</body>
</html>