<nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Ayam Penyet Sidoharjo</a>
            </div>
            <ul class="nav navbar-nav">
                <!-- <li class="active"><a href="Pesanan.html">Pesanan</a></li> -->
                <li><a href="menu.php">Menu</a></li>
            <?php
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $sesi_user= isset($_SESSION['member']) ? $_SESSION['member'] : NULL;
                if ($sesi_user != NULL || !empty($sesi_user))
                {

            ?>
                 <li><a href="top_up.php">Top-Up</a></li>
                 
                 <?php
                }else{
            ?>

                    <li><a href="#" onclick="top_up()">Top-Up</a></li>
            <?php        
                }
            ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <?php
                $sesi_user= isset($_SESSION['member']) ? $_SESSION['member'] : NULL;
                if ($sesi_user != NULL || !empty($sesi_user))
                {
            ?>
                    <li class="dropdown">    
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account
                            <b class="caret"></b>
                        </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="navbar-content">
                                        <div class="row">
                                            <div class="col-md-5">
                                            <?php 
                                                // $r=mysql_fetch_array($sql_user);
                                                if($_SESSION['member']['photo']==null||$_SESSION['member']['photo']==''){
                                            ?>
                                                <img src="http://placehold.it/120x120" alt="Alternate Text" class="img-responsive" />
                                            <?php
                                                }else{
                                            ?>
                                                <img src="Photo/user_<?php echo $_SESSION['member']['id']?>/<?php echo $_SESSION['member']['photo']?>" name="aboutme" width="120" height="120" class="img-circle"></a>                            
                                            <?php
                                                }
                                            ?>
                                                <p class="text-center small">
                                                    <a href="change_picture.php">Change Photo</a>
                                                </p>
                                                <a style="margin-left:10px;margin-bottom:10px;" href="profile.php" class="btn btn-primary btn-sm active">View Profile</a>
                                                <!-- <br> -->
                                            </div>
                                            <div class="col-md-7">
                                                <span><?php echo $_SESSION['member']['nama_depan'].' '. $_SESSION['member']['nama_belakang'];  ?></span>
                                                <p class="text-muted small">
                                                ID: <?php echo $_SESSION['member']['id']; ?></p>
                                                <div class="divider">
                                                </div>
                                                <span><?php echo 'Rp '.number_format($_SESSION['member']['saldo'],0,',','.');?></span>
                                                <div class="divider">
                                                </div>
                                                <?php
                                                    $item_total=0;
                                                    $item_jumlah=0;
                                                    if(!empty($_SESSION["cart_item"])){
                                                        foreach ($_SESSION["cart_item"] as $item){
                                                                    $item_total  += ($item["harga"]*$item["jumlah"]);
                                                                    $item_jumlah += $item["jumlah"];
                                                                }    
                                                    }
                                                ?>
                                                <a style="margin-left:10px;margin-bottom:10px;" href="cart_pesanan.php" class="btn btn-success btn-sm active cart-btn">Cart <?php if(!isset($_SESSION['cart_item'])){echo '0'; }else{ echo $item_jumlah; }?></a>
                                                <a style="margin-left:10px;margin-bottom:10px;" href="riwayat.php" class="btn btn-success btn-sm active">Riwayat</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="navbar-footer">
                                        <div class="navbar-footer-content">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="change_password.php" class="btn btn-default btn-sm">Change Passowrd</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="function/member/member_logout.php?logout=true" class="btn btn-default btn-sm pull-right">Sign Out</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                <!-- <li>
                    <p class="navbar-text"><?php //echo $_SESSION['member']; ?> <a href="function/member/member_logout.php?logout=true"><b>Logout</b></a></p>
                </li> -->
            <?php
                }else{
            ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form" role="form" method="post" action="function/member/member_login.php" accept-charset="UTF-8" id="login-nav">
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                            <input type="email" name="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                            <div class="help-block text-right"><a href="Forgpass.php">Lupa password ?</a></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="login" class="btn btn-primary btn-block">Masuk</button>
                                        </div>
                                        <div class="checkbox">
                                            <!-- <label>
											 <input type="checkbox"> Biarkan saya tetap masuk
											 </label> -->
                                        </div>
                                    </form>
                                </div>
                                <div class="bottom text-center">
                                    Baru Disini ? <a href="Register.php"><b>Daftar</b></a>
                                </div>
                            </div>
                        </li>
                       
                    </ul>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
    </nav>