<?php 
    include_once 'function/connect.php';    
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>

<body>
    <!-- PHP Code naviagasi user -->
    <?php include_once "navigation_user.php"; ?>
    <?php include_once "cek_user.php"; ?>
    <?php include_once "function/member/member_profile.php"; ?>
    <?php include_once "function/member/member_riwayat.php"; ?>

    <div id="exTab2" class="container">	
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3" style="margin-bottom:20px;">
            <div class="panel-body " style="background-color:white;">
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a  href="#1" data-toggle="tab">Riwayat Pesanan</a>
                        </li>
                        <li>
                            <a href="#2" data-toggle="tab">Riwayat Top Up</a>
                        </li>
                    </ul>

                    <div class="tab-content ">
                        <div class="tab-pane active" id="1">
                        <div class="row">
                                <div class="col-md-12" style="height: 400px !important;overflow-y: scroll;">
                                    <table>
                                        <?php
                                             $sqlRiwayatPesanan=getRiwayatPesanan();
                                             $no=1;
                                             while ($queryRiwayatPesanan=mysql_fetch_array($sqlRiwayatPesanan)) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <div  class="row" style="margin-top:10px; margin:3px; border:0.5px solid #f5f5f5 !important; ">
                                                            <div class="col-md-12">
                                                                <!-- Head Status -->
                                                                <div class="col-md-6">
                                                                
                                                                    <span style="color:#000000;font-weight:600">Date <?php echo $queryRiwayatPesanan['tanggal_pesan']; ?></span>
                                                                </div>
                                                                <div class="col-md-6"> 
                                                                    <?php
                                                                        if($queryRiwayatPesanan['id_status_pesanan']=='1'){
                                                                    ?>
                                                                        <span class="label pull-right" style="background-color:#ffd600;font-weight:600;font-size:13px;"><?php  echo strtoupper($queryRiwayatPesanan['nama_status_pesanan']);?></span>
                                                                    <?php    
                                                                        }else{
                                                                    ?>
                                                                        <span class="label pull-right" style="background-color:#00c853;font-weight:600;font-size:13px;"><?php  echo strtoupper($queryRiwayatPesanan['nama_status_pesanan']);?></span>

                                                                    <?php
                                                                        }
                                                                    ?>

                                                                </div>
                                                                <div class="col-md-12" style="margin-top:10px; border:0.5px solid;" ></div>
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top:5px;">
                                                                <?php  
                                                                    if($queryRiwayatPesanan['nama_tipe_pesanan']=='dine in'){
                                                                        
                                                                        $sqlDineInTable=getDineIn($queryRiwayatPesanan['id_pesanan']);
                                                                        while ($queryDineInTable=mysql_fetch_array($sqlDineInTable)) {
                                                                ?>
                                                                    <div class="col-md-9">
                                                                        <span> Jam Datang </span><span style="margin-left:10px;color:#000000;font-weight:600;font-size:12px;">: <?php echo $queryDineInTable['jam_datang'];?></span>
                                                                        <br>
                                                                        <span> Jumlah Tamu </span><span style="margin-left:2px;color:#000000;font-weight:600;font-size:12px;">: <?php echo $queryDineInTable['jumlah_tamu'];?></span>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="pull-right">Dine In</label>
                                                                    </div>
                                                                <?php 
                                                                        }
                                                                    }elseif($queryRiwayatPesanan['nama_tipe_pesanan']=='take away'){
                                                                         
                                                                        $sqlTakeAwayTable=getTakeAway($queryRiwayatPesanan['id_pesanan']);
                                                                        while ($queryTakeAwayTable=mysql_fetch_array($sqlTakeAwayTable)) {
                                                               
                                                                ?>
                                                                     <div class="col-md-9">
                                                                        <span> Jam Ambil </span><span style="margin-left:10px;color:#000000;font-weight:600;font-size:12px;">: <?php echo $queryTakeAwayTable['jam_ambil_pesanan'];?></span>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <!-- <span class="pull-right">Take Away</span> -->
                                                                        <label class="pull-right">Take Away</label>                                                                        
                                                                    </div>
                                                                <?php
                                                                        }
                                                                    }else{
                                                                        $sqlDeliveTable=getDelive($queryRiwayatPesanan['id_pesanan']);
                                                                        while ($queryDeliveTable=mysql_fetch_array($sqlDeliveTable)) {
                                                                ?>
                                                                              <div class="col-md-9">
                                                                        <span> Jam Antar </span><span style="margin-left:10px;color:#000000;font-weight:600;font-size:12px;">: <?php echo $queryDeliveTable['jam_antar_pesanan'];?></span>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <!-- <span class="pull-right">Take Away</span> -->
                                                                        <label class="pull-right">Delivery</label>                                                                        
                                                                    </div>
                                                                <?php }
                                                                    }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top:5px;">
                                                                <div class="6">

                                                                </div>
                                                                <div class="6">
                                                                    <div class="pull-right">
                                                                        <?php
                                                                                if($queryRiwayatPesanan['nama_tipe_pesanan']=='take away'){
                                                                        ?>
                                                                            <a href="InvoiceTakeAway.php?id_pesanan=<?php echo $queryRiwayatPesanan['id_pesanan']; ?>" class="btn btn-default" >LIHAT INVOICE</a>&nbsp;
                                                                        <?php
                                                                                }elseif($queryRiwayatPesanan['nama_tipe_pesanan']=='dine in'){
                                                                        ?>
                                                                            <a href="InvoiceDineIn.php?id_pesanan=<?php echo $queryRiwayatPesanan['id_pesanan']; ?>" class="btn btn-default" >LIHAT INVOICE</a>&nbsp;
                                                                        <?php
                                                                                }else{
                                                                        ?>
                                                                            <a href="InvoiceDelivery.php?id_pesanan=<?php echo $queryRiwayatPesanan['id_pesanan']; ?>" class="btn btn-default" >LIHAT INVOICE</a>&nbsp;
                                                                        <?php
                                                                                }
                                                                        ?>
                                                                        <button class="btn btn-success" data-toggle="collapse" data-target="#pesanan<?php echo $no;?>">DETAIL</button>
                                                                    </div>
                                                                </div>

                                                                

                                                                <div class="col-md-12 collapse" id="pesanan<?php echo $no;?>">
                                                                    <div class="col-md-12" style="padding:0 !important;" >
                                                                        <h4>Detail Pesanan</h4>
                                                                    </div>
                                                                    <?php
                                                                          $sqlPesananDetail=pesananDetail($queryRiwayatPesanan['id_pesanan']);
                                                                          $jumlah=0;
                                                                          $total_bayar=0;
                                                                          while ($querySqlPesananDetail=mysql_fetch_array($sqlPesananDetail)) {
                                                                    ?>
                                                                    <div class="row" style="padding:0 !important;">
                                                                        <div class="col-md-6">
                                                                            <?php echo $querySqlPesananDetail['nama_menu']; ?>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="pull-right">
                                                                                <span style="margin-right:15px;"><?php $jumlah+=$querySqlPesananDetail['jumlah']; echo '@'.$querySqlPesananDetail['harga']." | ";echo $querySqlPesananDetail['jumlah']; ?></span><span><?php $total_bayar+= $querySqlPesananDetail['jumlah']*$querySqlPesananDetail['harga'];echo $querySqlPesananDetail['jumlah']*$querySqlPesananDetail['harga']; ?></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <?php    }
                                                                    ?>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            
                                                                        </div>
                                                                        <div class="col-md-6" style="padding:0 8px 0 0 !important;">
                                                                            <div class="pull-right">
                                                                                <span style="margin-right:15px;"><?php echo $jumlah; ?></span><span><?php echo $total_bayar; ?></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="col-md-6"></div>
                                                                <div class="col-md-6"></div>
                                                            </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php  $no++; }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="2">
                            <div class="row">
                                <div class="col-md-12" style="height: 400px !important;overflow-y: scroll;">
                                    <table>
                                        <?php
                                             $sqlRiwayatTopUp=getRiwayatTopup();
                                             while ($queryRiwayatTopUp=mysql_fetch_array($sqlRiwayatTopUp)) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <div  class="row" style="margin-top:10px; margin:3px; border:0.5px solid #f5f5f5 !important; ">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <span style="color:#000000;font-weight:600"><?php echo $queryRiwayatTopUp['tanggal_top_up'];?></span>
                                                            </div>
                                                            <div class="col-md-6"></div>
                                                        </div>
                                                        <div class="col-md-12" style="margin-top:5px;">
                                                            <div class="col-md-6">
                                                                <span style="color:#ff1744;font-weight:600">No VA :</span> <span style="color:#000000;font-weight:600"><?php echo $queryRiwayatTopUp['no_virtual_account'];?></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                            <?php
                                                                if($queryRiwayatTopUp['id_status_top_up']=='1'){
                                                            ?>
                                                                 <span class="label pull-right" style="background-color:#ffd600;font-weight:600"><?php  echo strtoupper($queryRiwayatTopUp['nama_status_top_up']);?></span>
                                                            <?php    
                                                                }else{
                                                            ?>
                                                                <span class="label pull-right" style="background-color:#00c853;font-weight:600"><?php  echo strtoupper($queryRiwayatTopUp['nama_status_top_up']);?></span>

                                                            <?php
                                                                }?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-6">
                                                                <span style="color:#546e7a;font-weight:600">Nominal: Rp <?php echo number_format($queryRiwayatTopUp['jumlah_top_up'],0,',','.');?></span>
                                                            </div>
                                                            <div class="col-md-6"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once "footer.php";?>
    
</body>
</html>
