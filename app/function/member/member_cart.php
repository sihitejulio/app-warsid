<?php

   include_once '../connect.php';    
   include "member_menu.php";
    session_start();
    // foreach ($_SESSION["cart_item"] as $item){  
    //     print_r($item);
    // }
   
    if(isset($_POST['add_cart'])){    
        if(!isset($_SESSION['member'])){
            echo 'dont_login';
        }else{
            $id_menu=$_POST['id'];
            $jlh    =$_POST['jumlah'];
            if(!empty($_POST['jumlah'])) {
	
                $sql_menu = getMenuId($id_menu);
                $row = mysql_fetch_assoc($sql_menu);
                $itemArray = array('menu_'.$row["id"]=>array('nama'=>$row["nama_menu"], 'id'=>'menu_'.$row["id"], 'jumlah'=>$jlh, 'harga'=>$row["harga"]));
                // print_r($itemArray);   
                if(!empty($_SESSION["cart_item"])) {
                    if(in_array('menu_'.$row["id"],array_keys($_SESSION["cart_item"]))) {
                        foreach($_SESSION["cart_item"] as $k => $v) {
                                if('menu_'.$row["id"] == $k) {
                                    if(empty($_SESSION["cart_item"][$k]["jumlah"])) {
                                        $_SESSION["cart_item"][$k]["jumlah"] = 0;
                                    }else{
                                        $_SESSION["cart_item"][$k]["jumlah"] += $jlh;
                                    }
                                    // print_r($_SESSION["cart_item"][$k]);
                                }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
        }
    }

    if(isset($_POST['tambah'])){
        $id_menu=$_POST['id'];
        $jlh    =$_POST['jumlah'];

        if(!empty($_SESSION["cart_item"])) {
            $_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"]+=$jlh;
            $harga   = $_SESSION["cart_item"]['menu_'.$id_menu]["harga"]*$_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"];
            $nominal = "Rp . ". number_format($harga,0,',','.');
            $total=0;
            $total_bayar=0;
            foreach ($_SESSION["cart_item"] as $item){
                $total += $item["jumlah"];
                $total_bayar+=($item['jumlah']*$item['harga']);
            }    
            echo json_encode(
                array(
                    'item'=>'menu_'.$id_menu,
                    'jumlah_item'=>$_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"],
                    'jumlah_cart'=>$total,'nominal'=>$nominal,
                    'total_bayar'=>"Rp . ". number_format($total_bayar,0,',','.')
                )
            ); 
        }
    }

    if(isset($_POST['catatan'])){
        $id_menu    = $_POST['id'];
        $message    = $_POST['message'];

        if(!empty($_SESSION["cart_item"])) {
            $_SESSION["cart_item"][$id_menu]["catatan"]=$message;
        }
    }

    if(isset($_POST['kurang'])){
        $id_menu=$_POST['id'];
        $jlh    =$_POST['jumlah'];

        if(!empty($_SESSION["cart_item"])) {
            // print_r($_SESSION["cart_item"][$id_menu]["jumlah"]);
            $total=0;
            $total_bayar=0;
           
            if($_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"]==0){
                unset($_SESSION["cart_item"]['menu_'.$id_menu]);            
            }else{
                $_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"]-=$jlh;
                foreach ($_SESSION["cart_item"] as $item){
                    $total += $item["jumlah"];
                    $total_bayar+=($item['jumlah']*$item['harga']);
                }
    
                if($_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"]==0){
                    echo json_encode(
                        array(
                            'item'=>'menu_'.$id_menu,
                            'jumlah_item'=>$_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"],
                            'jumlah_cart'=>$total,
                            'nominal'=>0,
                            'total_bayar'=>"Rp . ". number_format($total_bayar,0,',','.')
                        )
                    );   
                    unset($_SESSION["cart_item"]['menu_'.$id_menu]);   
                           
                }else{
                    $harga=$_SESSION["cart_item"]['menu_'.$id_menu]["harga"]*$_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"];
                    // echo "Rp . ". number_format($harga,0,',','.');                        
                    $nominal = "Rp . ". number_format($harga,0,',','.');
                        
                    echo json_encode(
                        array(
                            'item'=>'menu_'.$id_menu,
                            'jumlah_item'=>$_SESSION["cart_item"]['menu_'.$id_menu]["jumlah"],
                            'jumlah_cart'=>$total,
                            'nominal'=>$nominal,
                            'total_bayar'=>"Rp . ". number_format($total_bayar,0,',','.')
                        )
                    );   
                }                    
            }
		}
    }

    if(isset($_POST['bayar'])){
        $email = $_SESSION['member']['email'];
        $sql   = mysql_query("SELECT * FROM member WHERE email='$email'");
        $member = mysql_fetch_assoc($sql);

        
               
        if($member['saldo']=='0'){
            echo json_encode(array('saldo'=>'0'));    
        }else{
            if(!empty($_SESSION["cart_item"])){
                $total_bayar=0;
                foreach ($_SESSION["cart_item"] as $item){  
                    $total_bayar+=($item['jumlah']*$item['harga']);
                }
                if($member['saldo']<$total_bayar){
                    echo json_encode(array('saldo'=>'tidak_cukup'));    
                }else{
                    // insert pesanan
                    $id_member         = $_SESSION['member']['id'];
                    $id_status_pesanan = '1';
                    $tipe_pesanan      = $_POST['tipe_pesanan'];
                    $tanggal=date('Y-m-d H:i:s');
                    
                    $queryInsert=mysql_query("INSERT INTO pesanan (id_member,id_tipe_pesanan,tanggal_pesan,id_status_pesanan,status_konfirmasi_user)
                    VALUES('$id_member','$tipe_pesanan','$tanggal','$id_status_pesanan','0')");
                    
                    $last_id_q = mysql_query("SELECT * FROM pesanan ORDER BY id DESC LIMIT 0, 1"); //id_pesanan
                    $last_id_res=mysql_fetch_assoc($last_id_q)['id'];                    
                    $queryInsertBayar=mysql_query("INSERT INTO pembayaran (id_pesanan,total_bayar,status_pembayaran)
                    VALUES('$last_id_res','$total_bayar','2')");

                    $saldo_sekarang=$member['saldo']-$total_bayar;
                    $email=$member['email'];
                    mysql_query("update member set saldo='$saldo_sekarang' where email='$email'");
                    

                    // insert pesanan detail
                    foreach ($_SESSION["cart_item"] as $item){ 
                        $jumlah           = $item['jumlah'];
                        $harga            = $item['harga'];

                        
                        if(isset($item['catatan'])){
                            $catatan   = $item['catatan'];
                        }else{
                            $catatan   = "";
                        }
                        $id_menu= substr($item['id'],5, strlen($item['id']));
                        
                        // $catatan_tambahan = $item['catatan_tambahan'];
 
                        $queryInsertDetail=mysql_query("INSERT INTO pesanan_detail (jumlah,harga,id_pesanan,id_menu,catatan_tambahan)
                        VALUES('$jumlah','$harga','$last_id_res','$id_menu','$catatan')");
                        unset($_SESSION["cart_item"]['menu_'.$id_menu]);            
                    }

                    // insert dine in or take away detail

                    if($_POST['tipe_pesanan']=='1'){
                        $dine=date('Y-m-d').' '.$_POST['dine'].':00';
                        $tamu=$_POST['tamu'];
                        $queryInsertDin=mysql_query("INSERT INTO dine_in_table (id_pesanan,jam_datang,jumlah_tamu)
                        VALUES('$last_id_res','$dine','$tamu')");
                    }elseif($_POST['tipe_pesanan']=='2'){
                        $take=date('Y-m-d').' '.$_POST['take'].':00';
                        $queryInsertTaw=mysql_query("INSERT INTO take_away_table (id_pesanan,jam_ambil_pesanan)
                        VALUES('$last_id_res','$take')");
                    }
                    elseif($_POST['tipe_pesanan']=='3'){
                        $order_delivery=date('Y-m-d').' '.$_POST['order_delivery'].':00';
                        $no_hp=$_POST['no_hp'];
                        $alamat=$_POST['alamat'];
                        $ongkir=$_POST['ongkir'];
                        $queryInsertTaw=mysql_query("INSERT INTO delivery_table (id_pesanan,jam_antar_pesanan,no_hp,alamat_antar,ongkir)
                        VALUES('$last_id_res','$order_delivery','$no_hp','$alamat','$ongkir')");
                    }
                    echo json_encode(array('status'=>'sukses'));                       
                }
            }
        }

    }
?>