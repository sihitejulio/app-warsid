<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include_once "head.php"; ?>
</head>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->

<body>
<?php include "navigation_user.php"; ?>

<div class="container">
    <br>
	<div class="well">
		<h2 class="text-divider"><span>Jam Operasional</span></h2>
		<div class="text-center">
                <p>No Telepon</p>
                <div>
                    <p>061-xxxxxxxx <br>
                    081xxxxxxxxx</p>
                </div>
                <p>Email</p>
                <div>
                    <p>xxxxxxxxx@gmail.com <br>
                    xxxxxxxx@yahoo.com</p>
                </div>
                
        </div>
	</div>
</div>
<?php include "footer.php"; ?>
</body>
</html>